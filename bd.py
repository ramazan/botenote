import config
import sqlite3


def insert_data(key, value, user, date):
	conn = sqlite3.connect(config.base_name)
	c = conn.cursor()
	c.execute('INSERT INTO data(key, value, user, date) VALUES(?, ?, ?, ?)', (key, value, user, date,))
	conn.commit()
	conn.close()


def return_value(key, user):
	conn = sqlite3.connect(config.base_name)
	c = conn.cursor()
	values = []
	for element in c.execute('SELECT * FROM data WHERE (data.key == ? and data.user == ?)', (key,user,) ):
		values.append(element[1]+', '+str(element[-1]))
	conn.close()
	return values

def last_ten(user):
	conn = sqlite3.connect(config.base_name)
	c = conn.cursor()
	values = []
	for element in c.execute('SELECT * FROM data WHERE (data.user == ?) order by date desc limit 10', (user,)):
		values.append(element[0] +' - '+element[1]+', '+ str(element[-1]))
	conn.close()
	return values

def show_all(user):
	conn = sqlite3.connect(config.base_name)
	c = conn.cursor()
	values = []
	for element in c.execute('SELECT * FROM data WHERE (data.user == ?)', (user,)):
		values.append(element[0]+' - '+element[1] + ', '+str(element[-1]))
	conn.close()
	return values

def delete(id, user):
	conn = sqlite3.connect(config.base_name)
	c = conn.cursor()
	c.execute('DELETE FROM data WHERE (data.id == ? and data.user == ?)', (id, user, ))
	conn.commit()
	conn.close()

def getById(id, user):
	conn = sqlite3.connect(config.base_name)
	c = conn.cursor()
	values = []
	for res in c.execute('SELECT * FROM data WHERE (data.id == ? and data.user == ?)', (id,user,)):
		values.append(res[0] + ' - ' + res[1] + ', deleted')
	conn.close()
	return values

def all_keys(user):
	conn = sqlite3.connect(config.base_name)
	c = conn.cursor()
	keys = []
	for element in c.execute('SELECT * FROM data WHERE (data.user == ?)', (user,)):
		if (element[0] not in(keys)):
			keys.append(element[0])
	conn.close()
	return keys
