# DOCUMENTATION

## Libraries
```
pip install pytelegrambotapi
```
## How to use
- save notes typing: ```key``` - ```value```
- print values typing: ```key```. The result will be - ``` value, id```

## Commands
- /show_all - showing all notes, that were added
- /show_last_ten - showing last notes
- /show_all_keys - show all keys
- /delete [number] - delete element with id = number


