import sqlite3
import config

conn = sqlite3.connect(config.base_name)

c = conn.cursor()

c.execute('''create table data (key text, value text, user integer, date DATE, id integer primary key) ''')

conn.commit()
conn.close()