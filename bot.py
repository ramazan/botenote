import telebot
import config
import bd
import utils

bot = telebot.TeleBot(config.token)
 
@bot.message_handler(commands=['show_last_ten'])
def show_last_ten(message):
	for element in bd.last_ten(message.from_user.id):
		bot.send_message(message.chat.id, element)

@bot.message_handler(commands=['show_all'])
def show_all(message):
	for element in bd.show_all(message.from_user.id):
		bot.send_message(message.chat.id, element)

@bot.message_handler(commands=['test'])
def test(message):
	bot.send_message(message.chat.id, 'sdsd')

@bot.message_handler(commands=['delete'])
def delete(message):
	text = utils.text_after_first_space(message.text)
	if (utils.is_number(text)):
		id = int(text)
		for element in bd.getById(id, message.from_user.id):
			bot.send_message(message.chat.id, bd.getById(id, message.from_user.id))
		bd.delete(id, message.from_user.id)
	else:
		bot.send_message(message.chat.id, "can`t delete "+text)

@bot.message_handler(commands=['show_all_keys'])
def show_keys(message):
	for key in bd.all_keys(message.from_user.id):
		bot.send_message(message.chat.id, key)

@bot.message_handler(commands=['show_my_id'])
def show_my_id(message):
	bot.send_message(message.chat.id, message.from_user.id)

@bot.message_handler(commands = ['to_partner'])
def to_partner(message):
	text = utils.text_after_first_space(message.text)
	splited_text = text.split(" ")
	if (utils.is_number(splited_text[0])):
		text2 = utils.text_after_first_space(text)
		words = text2.split('-')
		if (len(words) == 2):
			bd.insert_data(words[0], words[1], message.from_user.id, message.date)
			bd.insert_data(words[0], words[1], int(splited_text[0]), message.date)

@bot.message_handler(content_types=["text"])
def body(message):
	words = message.text.split('-')
	if (len(words) == 2):
		bd.insert_data(words[0], words[1], message.from_user.id, message.date)
	if (len(words) == 1):
		for value in bd.return_value(words[0], message.from_user.id):
			bot.send_message(message.chat.id, value)

if __name__ == "__main__":
	bot.polling(none_stop=True)