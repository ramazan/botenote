import sqlite3
import config

conn = sqlite3.connect(config.base_name)

c = conn.cursor()

c.execute('''drop table data''')

conn.commit()
conn.close()