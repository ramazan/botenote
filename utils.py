def is_number(str):
	try:
		int(str)
		return True
	except ValueError:
		return False

def text_after_first_space(str):
	index = str.find(" ")
	if (index != -1):
		return str[index+1:]
	return str

def get_partners(str):
	res = []
	str2 = str.split(" ")
	for element in str2:
		res.append(int(element))
	return res